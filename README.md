# Blog
Small texts, notes from articles, talks, topics.  

available on https://qwde.no/blog

# Setup
[Hakyll](https://jaspervdj.be/hakyll/) for blog engine. Read more about it on [my blog if you want to](http://qwde.no/blog/hakyll-for-website-hosting.html). The most imporant thing to know is that Hakyll is a static site generator, meaning it compiles content in `src` directory into a static directory, meaning it works without using a backend server. It just generates a bunch of html and/or css/javascript.

### Building
I gave up nix which I used before, which still seems popular. `stack` for haskell also seems difficult and uncessary over regular `cabal`.
So I recommend that you install cabal _somehow_ (they seem to keep changing the default bundler) and then
```bash
cabal new-run site build
```
and you'll see an output-directory somewhere. You can also check the `Jenkinsfile` here
