---
title: Bash Tips and Tricks
---

When I was in uni I worked long hours. I wasn't self-aware enough to realize there eventually came many digressions from my fatigue. One of them was Vim, the other was just regular terminal usage. So now I now a lot of shortcuts. I've unlearnt a few, since I came to realize that knowing too many shortcuts puts a toll on your head. After long computer sessions I would "see" shortcuts in real life, like thinking `Ctrl+d` when someone exited the room. 

Bash shortcuts are both simple and tedious to understand. Tedious because some commands are ignored in your bash history and history overwriting is an issue. And to be honest, I'm to lazy to have learnt all the rules myself properly. I just have a rough intuition.

# Shortcuts

## Ctrl+r (reverse-i-search)
The first thing I recommend to anyone using a terminal is `ctrl+r`. I couldn't imagine using a terminal as much as I do without that shortcut. Ctrl+r enters an interactive search. Each key you type in will search your history backwords for an exact match anywhere in each line. E.g. if you type `echo hello world`, `ctrl+r` and then `world`, you'll get back the full command `echo hello world` in your current buffer. Neat. You can also play around with ctrl+s (after ctrl+r) and read more online. However, rather recommend to use [fzf](https://github.com/junegunn/fzf) to juice up `ctrl+r`. It adds another keystroke to select the right command, but the fuzzy searching, visual inspection of matched lines makes it all worth it. While you're at it, make sure to enable **fuzzy file search**. Combined with [fd](https://github.com/sharkdp/fd) it lets you quickly navigate to files and/or directories. I've modified my `cd` command so that it will accept _files_ as arguments, making it easy to `cd` into a directory using fuzzy finders (or plain' ol' `find`). You can see my full `cd` script here: https://github.com/andsild/dotfiles/blob/master/Bash/Sourced/cd
It's bloated and also contains a command to clear the screen and type `ls` each time you change directory. Aaand there's a `cd` history that let's you scroll back (using [cdm](https://github.com/andsild/dotfiles/blob/master/Bash/Sourced/cdm) and [pd](https://github.com/andsild/dotfiles/blob/master/Bash/Sourced/pd)).

## Ctrl+w, ctrl+u
The former removes a word, the latter removes the whole line. Speeds things up

## Ctrl+e and Ctrl+a
Go the end of the command line and the front, respectively. Useful when you need to navigate back and forth! While you're at it, teach yourself alt+b and alf+f, too, which can go [B]ack to a word or [F]orwards. It took me a little while to learn, but now I use it all the time without thinking about it. These are by the way emacs-bindings, which in fact has inspired a lot of bash shortcuts. If that pisses you off, you can modify you /etc/inputrc to have vim bindings. I've tried, and I am avid vim user, but I have to say I don't recommend it. If the idea of Vim and a terminal attracts you I'd rather use Neovim and it's builtin `:terminal`. Or teach yourself `screen`'s keybindings (e.g. `ctrl+[` which enables copy mode). I know both, which makes me question myself sometimes...

## Ctrl+l
Clear the screen. Feels like the kind of shortcut most people build into their muscle memory pretty fast.

## Escape+dot
Get the last word of the command you ran. Takes some time to learn, but surprisingly useful. Type escale+dot more times to cycle!

# Flow control
## !!
When you run this in a shell, it will expand after you hit enter to be the string of the last command you ran. Typical usage is `sudo !!`.

## !commmand:<word>
This is getting way more advanced, so I seldom use it these days. But `!command` will expand to the first match in your history matching the string. E.g. `ls my-directory<enter> !ls` will list _my-directory_ twice. You can get advanced and add the colon, where you can choose which word to get from the last command. E.g. you type in `ls my-directory<enter>`, then `echo I just viewed !ls:1` will print `I just viewed my-directory`.

## Ctrl+z
We giveth processes life in our terminals, but we may equally taketh it. Ctrl+c is the conventional way, but you may want the process to _sleep_ instead. This could be because the process is (rudely?) not responding to SIGINT  (i.e. ctrl+c) or because you want the process to be forked to the background. To experiment, type `sleep 100`, then ctrl+z. You may now inspect the background process with `jobs -l`. This lists the PID and command you ran. You may now choose to `kill -9 <PID>` or type `fg` (ForeGround) to put the command back. `bg` (BackGround), however, puts the command in the background. You may now continue to use the shell interactively while your process is running.


# Not super useful in my opinion, but I know them anyway
## Ctrl+j
If you don't want to move your pinkie-finger off the index row aaaaall the waaaaay to the enter key, `ctrl+j` is your friend.

## Ctrl+h
Backspace. Useful when your backspace key for some reason doesn't work or you spilled something on your keyboard. Or if you're having a period of RSI in your right hand. I taught myself this shortcut by unbinding backspace both in Vim and inside the terminal (by modifying /etc/inputrc). However, much typing in Word and Google Docs quickly "unlearnt" this shortcut for me. Better to learn how to type properly!
