---
title: SSH Through an Android Proxy
---

*sigh*.

My company protects all of it's network nodes with a set of firewalls. We can get through with a VPN accessible through Cisco AnyConnect. There are Windows, Mac, Linux and Android clients, but unfortunetaly, I use NixOS. The problem here is that the Linux anyconnect binary tries to find dependencies in `/lib/` directory, but NixOS has masked away those to other directories (`/nix/store/...`). It can be worked around (using a series of bashing the head against the wall following this (amazing) guide: https://unix.stackexchange.com/a/522823), but the procedure isn't guaranteed to produce results, so I'd rather rely on something else. And since the client is proprietary I'd rather not spend time on making a packaged version. There are open source tools like [openconnect](https://github.com/openconnect) but in my experience they don't have 2FA support.

Installing AnyConnect is easy on Android, so my phone can be of use. I installed [every proxy](https://play.google.com/store/apps/details?id=com.gorillasoftware.everyproxy&hl=en&gl=US) which allows me to forward connections _through_ the VPN. Several other proxy software only forwards the VPN-free internet connection. So yay to "every proxy". However, they only forward HTTP/S and SOCKS5. Welp. I tried configuring SSH on port 80 and 443, but to no avail. I wonder why, and really wished "every proxy" was open source.

I tried various "ssh over http" solutions but ended up bashing my head against the wall again. The final solution was a bit on the edge: I installed [cockpit](https://cockpit-project.org/), which has a web-based terminal emulator. They also allow 2FA, which is great. Then I have a terminal accessible through my web browser over (i.e. over HTTP/S protcol).

The biggest problem with a web-based terminal browser (outside of security) is that terminal shortcuts do not function well in web-browsers. E.g. Ctrl+W, which I use all the time for removing a word, closes a tab in a web browser. Disabling shortcuts in Firefox and Chrome is surprisingly difficult. There are plugins that can help you disable _some_ of them, but you're likely to turn mad. Thankfully, I had knowledge of [qutebrowser](https://www.qutebrowser.org/). This is arcane software, but it has a "passthrough mode" (enabled with `ctrl+v`) which disables all shortcuts.

And there you have it. I'm not sure if I'm proud or ashamed.

# Alternative: VPN through VM
I downloaded an Ubuntu VM in VirtualBox. It's also something of a hassle, but it doesn't require any modified workflows once it's setup, so the end result is less annoying. In the VM I installed the VPN software. For SSH forwarding through the VPN you can use plain openSSH (`apt install openssh-server`). For non-ssh connections I used [Squid](http://www.squid-cache.org/) (`apt install squid`) and added the following lines above the `http_access deny all` segment in the configuration:
```
...
acl all src 0.0.0.0/0
http_access allow all
...
```

Then add a port forward in the virtual machine (allowing connections from hosts to the Squid server) and voila - you can now forward both SSH and HTTP/S through the VM. I use `google-chrome-stable --proxy-server=192.168.56.1:3128` for HTTP/S and `ssh -AJ username@<vpn_ip>:3022 <remoteuser>@remotehost` for SSH, where 3022 is the port I used in the forwarding rules in the virtual machine.

I find virtual machines and laptops to be somewhat unreliable, so I recommend a snapshot once everything is setup to save yourself some time. In VirtualBox you can use
