---
title: Quotes on Programming
---

Chesterton's fence: if you meet a fence and you don't cannot explain why it is there, don't remove it. Don't come back until you understand.

Akins' 36th law: Any run-of-the-mill engineer can design something which is elegant. A good engineer designs systems to be efficient. A great engineer designs them to be effective.

Akin's 5th law (citing Miller): Three points determine a curve. 

A model is a lie that helps you see the truth - Howard Skipper

... "the ostrich algorithm is a strategy of ignoring potential problems on the basis that they may be exceedingly rare"

"Developers are drawn to complexity like moths to a flame, often with the same outcome." – The Productive Programmer by Neal Ford

... "There is leadership in the sense of deference, just as people defer to Linus Torvalds. But the moment people stop respecting Torvalds, they can fork it" - Justine Tunney

It's okay to swear in the code, since it lets other developers know where the difficult parts are.

Managing complexity, runtime and dependencies are useful skills in programming, managing your energy and motivation are essential skills.

The Programmers' Credo: we do these things not because they are easy, but because we thought they were going to be easy

If you can't learn to do it well, learn to enjoy doing it badly

The worst thing about Jenkins is that it works. It can meet your needs. With a liiittle more effort, or by adopting sliiiightly lower standards, or with a liiiiitle more tolerance for pain, you can always get Jenkins to do aaaaalmost what you want it to - "https://twitchard.github.io/posts/2019-06-21-life-is-too-short-for-jenkins.html"

Judith Grabiner: “First the derivative was used, then discovered, explored and developed, and only then, defined.”)"



<a href="https://spacecraft.ssl.umd.edu/akins_laws.html">Check out the rest of Akin's laws of spacecraft design here</a>
