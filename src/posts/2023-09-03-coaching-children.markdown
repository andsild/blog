---
title: Coaching Children
---

This weekend I took a course on principles of coaching children. I've been a gymnastics instructor for the past 1.5 years now and additionaly about a year when I was 16-17. Here's my main takeaways from the course:

# Remember to play
Young children usually don't care as much about achieving X or Y as they do having fun. So start with many different games, and incorporate different elements into it to keep it challenging.
If you are teaching them anything technical, big bonus points if you manage to incorporate it into a fun game later in the session.

Maybe a reason many fall of after the age of 12 is that they don't play as much as they used to?

# Remember to cooperate
When asking a lot of athletes what they'll remember and appreciate most after their career, they usually talk about friends they made. You can help them get there. Make sure to have excercises that incorporates collaboration. It could be games where they have to hold hands whilst running obstacles or away from others. It could be solving puzzles together, like building a pyramid or solving puzzles of form "2 feet and 3 legs in the ground, NOW".

There are many other forms of collaboration, like getting the ones you are coaching to help _each other_ and not just taking feedback from you.

# Remember to communicate
It's important for everyone to be seen. Ask children about their family, goals, past, future, etc. If anyone is falling behind or anything similar make sure to talk to them.

# Remember to autonomize
As an instructor you need to provide structure and insist on certain things, but allowing children to pick some of their own excercises or games drives motivation.

# Remember to commend
If a kid does something incorrectly, telling them what they did wrong is not that motivational to continue with.
As an example, I sometimes notice that children I coach _think_ correctly even if they don't get the excercise right. I usually commend them for having the right mindset and let them try again.

With children, being talented is not goal number \#1. So as a coach you may want to commend behaviours that you like, such as noticing that someone is in a good mood, show up early or that they are kind to each other. By endorsing these behaviours you'll have a much nicer environment to be in :)
