---
title: The Physilogy of Taste
---

<img src="https://upload.wikimedia.org/wikipedia/commons/c/c9/Jean_Anthelme_Brillat-Savarin.jpg"
  alt="savarin book cover"
  style="float: left; margin-right: 25px; max-width: 200px;" />


Jean Anthelme Brillat-Savarin apparently wrote one of the most famous foodie books anonymously two months before his death in 1826. So I decided to give it a go.

The book starts with what I can only call the most French thing ever: defining sex-drive as the sixth sense. It is stated as science in a layman's language with a lot of personality that carries throughout the book. Other quotes that you might have heard of also appear throughout: "_Dis-moi ce que tu manges, je te dirai ce que tu es._" - tell me what you eat, and I will tell you who are.

Some concept in the book I appreciate a lot: Savarin defined food with _all_ senses. Taste and smell might seem obvious, but Savarin makes it clear that _sight_ is as apparent. Sound I'm not so sure of myself but _touch_ - the consistency of the food, is also important! I'm not sure but I get the feeling Savarin might have been one of the first to define this in a quasi-scientific fashion which is part of the joy with this book.

Savarin also goes to great lengths to define a dining experience: how to compose a good company, how to greet guests, the importance of being on time and the pleasure of the dessert. Sometimes the book feels like an homage to dinner parties. If you enjoy them too, then I'm sure you'll enjoy the book.

There are some hilarious anecdotes in the book: a man who once ate a whole turkey by himself and flushed it down with red wine. The story that stands out the most for me is about caffeine. Apparently it was an exotic good that would only come in sometimes. Savarin talks about how one might end up not getting sleep. This has happened a lot to me: I'm quite sensitive to caffeine. The fascinating part to me is Savarin defines this as a _pleasurable_ experience. Savarin talks about the joys of having energy and a lot of thoughts. I love his take on this - much more life-enjoying than my usual annoyances of losing productivity.

Overall a fun read!
