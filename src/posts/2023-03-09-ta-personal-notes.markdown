---
title: Reflections on being CS TA
---

The format of my TA-experience in my PhD is that I show up for class, and am then expected to help the students through their homework assignments. We also correct them after delivery.
Here I've written down some personal notes that I keep in mind.

# Be explicit
Use _progression_. Initially assume little. A good way to make sure you're at their level is to ask a question that the student can answer with confidence. _Then_ build up.

# Use GUIs
I've always loved the command line over anything, but not everyone shares that joy. And its 2023 now, so accept GUI solutions.

# Make autotests
Have a student that is stuck or want to verify/improve their solution? There's many useful tools that can help, e.g. `valgrind` for testing C code, `telnet` and `curl` for testing HTTP servers, etc. I usually make some handy commands that works well for the assignments that students have.

# Relations
Many student show up and sit by themselves. Which is fine. But every once in a while you can/should make some laps to ask everyone how it's going. Some will dismissively say it's fine, but I always get a question or three when I approach them instead of the other way around. And then it might be easier for them to approach you later.

# Collaboration
A lot of students come in groups and are happy to sit there, but if you can think of anyway to get to collaborate, sitting in pairs or looking at something together, thats usually a bonus :-)
