---
title: Haskell Gotchas
---

A book called [The Haskell School of Music](https://www.cs.yale.edu/homes/hudak/Papers/HSoM.pdf) has this on page 4:

```haskell
5 ∗ (−0.123456 + 0.123457) :: Float ⇒ 4.991889e − 6
5 ∗ (−0.123456) + 5 ∗ (0.123457) :: Float ⇒ 5.00679e − 6
```
Floating point rounding errors aren't unique to Haskell of course, but I still think it's curious with a sample of how it can go wrong.


Another "gotcha" is Haskell's default string: they're (still) linked lists. And you wouldn't think much of it, until you see the performance. My most efficient optimization for my master's thesis (which I've partially kept on [my github profile](https://github.com/andsild/HaskellPageRank)) was [converting input reading to be `Data.Text`](https://github.com/andsild/HaskellPageRank/blob/master/executables/Main.hs#L130) instead of default strings. For large inputs it made a big difference. If you take a gander at the code you'll also notice that it's heavy with strict evaluation - as opposed to Haskell's lazy-by-default design. Generally, when benchmarking I found this to be more efficient and also predictable if I was debugging something else.

Beyond that I'd like to say I love writing Haskell, albeit I devote less time for it now than before. Though, with the advent of AI, I find that it is getting easier. Give it a whirl!
