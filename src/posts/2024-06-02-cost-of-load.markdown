---
title: Cost of buying vs renting
---
Before buying an apartment, I wanted to validate that it actually payed off vs just renting one.
The cost of 1 000 000 NOK loan with different payment rates are:

| years | cost |
|----|-------|
| 20 | 1,583 |
| 25 | 1,753 |
| 30 | 1,789 |

... Which I read as: between 50 and 75% "excess" of your loan sum. Now, at least in Norway, there are some tax benefits from loaning. So on the 50-75% excess, around 20% of that "vanishes" in tax reductions. So the actual excess is smaller than it seems. Another important consideration is inflation rates. The loan is based on what you rent today. But eventually, your money will be worth more and the loan will be worth less. So another part of the excess also "vanishes" because of that.

I got some numbers off of a [news article from VG (Norwegian)](https://www.vg.no/nyheter/innenriks/i/GMbXrJ/derfor-er-inflasjonen-bra-for-deg-med-boliglaan). Their estimate is that a 4 million loan is reduced to 3,1 million within 5 years of 5% inflation, and 3,6 million with 2% inflation. So inflation isn't all bad if you have loans.

Nevertheless, there is a bottom line: you cannot predict the future. Any house purchase might be a lottery winner if your neighborhood appreciates in value. It can also turn out bad due to reputation, natural disasters, or loss of jobs in the area. The rough guideline I read multiple places is that it normally pays off to own vs rent after 10 years of normal economic conditions. 

With an investment as big as home ownership, its also wise to not just think of it as an investment, but also a place to stay. Not everything has to boil down to finance, it's also important to consider the neighborhood, proximity to nature, etc. This is getting more and more difficult to do, since the cost of buying your ``dream house'' is ever-increasing and you might be in a rush to buy a house to avoid donating your income to another landlord.  
