---
title: Jenkins tips and tricks
---

# Using Jenkins
Jenkins warns against using "built-in nodes", but at the same time, this simplifies a lot. It's not sexy, but spending less time on Jenkins is. Rather than obsessing about advanced functionality and how that will help you scale, I'd rather (in hindsight) focus on what is _good enough_ and then consider expanding eventually.

Generally I'd recommend always relying on pipelines if you want advanced functionality like changesets, etc. The GUI components are more limited to how much effort plugin developers put in.


## Running scripts WITH stderr in groovy console:
```bash
def cmd = ['/bin/sh',  '-c',  'ls -l /var/run/docker.sock']

cmd.execute().with{
  def output = new StringWriter()
  def error = new StringWriter()
  it.waitForProcessOutput(output, error)
  //check there is no error
  println "error=$error"
  println "output=$output"
  println "code=${it.exitValue()}"
}
```
## Integration with GitLab
GitLab is great. You could use them instead of Jenkins, but then risk paying some costs for hardware down the line, or miss out on paywall-blocked features.
Anyhow, to integrate your Jenkins status with GitLab, a common build pipeline for me looks like this:
```
      steps {
        updateGitlabCommitStatus name: 'build', state: 'pending'
        ... <build commands> ...
        updateGitlabCommitStatus name: 'build', state: 'success'
        }
```

If you have setup an integration this should work just fine. Setup Jenkins to build branches `origin/${gitlabSourceBranch}`, so you can configure build behaviour in GitLab.
