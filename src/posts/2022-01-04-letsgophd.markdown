---
title: After Five years of Software Development I'm Starting a PhD
---

The masters degree was really fun for me, I really enjoyed sitting all day with books and whatnot and spending long walks pondering different ideas and directions to take my project. That being "my story", I still did not manage to get a top-level grade on my thesis, which broke down my spirits. I thought maybe a life in academia was not meant for me. 

"Just for fun", I kept checking PhD positions around Norway. One day, me and a friend looked at listings together. He urged me to apply for some of them. I insisted no, there's no use, but he convinced me to do it anyway. And yeah, I was then called in for an interview. I knew the reject rate was high, so I was nervous. Then I remembered what I've heard several performers say over and over: being nervous is good, because it means you're doing something you care about. I did the interview. 

Few months later, I'm now moving from my apartment and getting ready to begin a new (old?) life in Tromsø!
