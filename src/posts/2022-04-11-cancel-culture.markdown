---
title: Is Cancel Culture Toxic?
---

The first question is whether or not cancel culture really exists. Arguments against cancel culture argue that they are not cancelling anyone, they are are actually shutting down racism and/or other forms of discrimination.

Garry Kasparov said "In both Russia and the US we have freedom of speech, but in the US, they also have freedom after speech". He also claimed "Universities is where we should challenge and be challenged, but how can we do that if we are afraid to be wrong". This is especially important such as in the case where a Norwegian professor in Bergen made a joke in reference to german tourists and WWII, stating that "the germans are coming". This resulted in a complaint which was supposed to be anonymous, but led to [massive media coverage](https://khrono.no/dette-er-tyskervits-saken/507918). The uproar in the media was largely due to whether such jokes are permitted or not and also how the complaint was handled. The initial response from the university was to apologize to the student and forbid the professor from future similar jokes. It thankfully ended with an apology from the university instead and a salary increase to the professor.
