---
title: Projects I like
---
This post contains some of the things I've started, but eventually left - that I might continue on a sunny day (but probably just leave alone):


[rtnz's blog](http://www.rntz.net/post/against-software-development.html) - I like this manifesto/outcry, particularly the bottom part. I've been thinking about writing something similar.

[Haskell's pytorch port](https://hasktorch.github.io/tutorial/06-linear-regression.html) - if you want to know what "fucky linear regression" looks like. No, seriously, this project is really fun, and the functional style of writing code has led to some personal insights for me about machine learning, especially the consideration of what a model iteration really means. Installing this projects was a little cumbersome for me, but it's fun once it works. 


Gamificiation of graph reductions. Interesting case where players would solve previously unsolved digital topology problems: [https://read.somethingorotherwhatever.com/entr1y/NiceNeighbours1](https://read.somethingorotherwhatever.com/entr1y/NiceNeighbours1)

Euclidean algorithm (combined with Bjorklond's algorithm) to generate rhythms: [http://cgm.cs.mcgill.ca/~godfried/publications/banff-extended.pdf](here)
