---
title: My personal notes for renting out a flat
---

# Tips I wish I knew when renting out an apartment

Take measure of everything in your apartment, because you might need to get them replaced. To avoid asking for measures of everything, it's convenient to compile a small list of measures that are sufficient for ordering replacements.

Another thing is to take note of how old your equipment is. Some appliances, e.g. dishwashers, have an expected lifespan of 10-15 years. If your tenant makes a complaint about them, its easier to agree or disagree when you know how far due your equipment is.

Keep in mind that there will be variable costs. Things will need to be replaced, then theres also snow plowing, water bills, etc. So now I hold off a certain amount of money each year for the variable costs.

The income from renting out your apartment is taxable. So even if you break even with renting out your own place and living another, your tax increases because of your higher income.
