---
title: Back to Software Basics 
---
_"How many bits are there in a byte?"_  
I was fresh out of the university and got this question during a job interview. I answered "8" hesitantly, wondering what trap lay ahead. The interviewer picked up on my hesitation and explained that some candidates forget the abc's of computer science, especially if
they've been in the industry a few years. They wanted to make sure I still could work with the basics. 

There's another popular more extensive "FizzBuzz" test that has been getting popular. The essense of the test is to check numbers 1..100 for division of the numbers 3 and 5. To me, the essense of test is the same as asking how many bits there are in a byte. For fizzbuss, you have to remember the modulus operator. Fizzbuzz became popular online because it turns out people who work in the industry rarely use it, even though just about all programming languages have it.

Forgetting things we have leart is natural. Even MIT students have been found in a study to forget some of their lessons from e.g. physics. 
I've experienced the same about programming many times. Recently I wrote a website using Java's built in sockets instead of using off-the-shelf frameworks. I had several problems; connections were left in `FIN_WAIT_2`, threading caused mysterious socket write exceptions and some other mumbo-jumbo. I both learned and re-learned a lot, and painfully realized I haven't always been a TCP expert. It also reminded me that not all off-the-shelf frameworks are bad. After switching to <a href="https://micronaut.io/">Micronaut</a> all the problems went away, and I had less code to worry about.

The lesson? Overall, we don't we need to remember everything to do what we do. But also, going back to where you started is also a great lesson in humility.
