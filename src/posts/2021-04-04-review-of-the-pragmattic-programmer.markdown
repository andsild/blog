---
title: The Pragmatic Programmer - Review
---

I've been wanting to improve and fully embrace my life as a software engineer. So I thought I'd get one of the highest rated books on the subject.  The Pragmatic Programmer is written by David Thomas and Andrew Hunt. David is a Musician by profession while Andrew has written several books on software development, including the "Agile Manifesto" The book was written originally in 1999, and then re-released in 2019. I've read the second version.

The book informally explains tips and tricks to do better in software engineering. It feels like a conversation in a pub. I.e. there is little explicit reference to research, although I am sure many of the conclusions are backed up by it. 

There are a few terms and concepts that I appreciated reading. "Tracer bullets", which is the idea of iteratively doing a _full_ iteration of development between each round of feedback (as opposed to doing one component at a time). Also, "orthogonal design" which is the idea that a change in one component should not affect the others (imagine vectors v1 [0, 1] and v2 [1, 0] with individual dimensions).

Some sections seem good for novice programmers, such as using version control, achieving "editor fluency" and breaking down problems to give good estimates. I am not convinced that either of these _really_ boost productivity; I've known great programmers that aren't champions in any of these domains. I therefore only skimmed these sections. Similarly, there are many sections about designing code by components, anti-patterns and TDD. These are definitely a worthy read if you've never seen these before, but I kind of wish these would be covered in a different kind of book. In "The Pragmatic Programmer" they give you enough information to see a pattern, but I feel there is not enough context to know when to _not_ apply a pattern and to think critically about benefits of clean design vs "getting it done". 


The narrative style of ``The Pragmatic Programmer`` makes the book quick and easy to read, although I do question many of their dogmatic suggestions: "Make sure to read one technical book each month" and "to do estimates, divide your problem into smaller pieces". These seem intuitive, so why doesn't everbody do it? I wish that a book titled "pragmatic" would discuss their suggestions more in detail, and maybe weigh in on pros and cons of good habits.

Overall I'd recommend the book to beginners, but for more experienced developers I'd probably suggest another book.

