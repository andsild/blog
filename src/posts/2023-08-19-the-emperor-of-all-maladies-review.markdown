---
title: Review of "The Emperor of All Maladies, A Biography of Cancer"
---

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/0_Siddhartha_Mukherjee_8126179.jpg/1024px-0_Siddhartha_Mukherjee_8126179.jpg"
  alt="savarin book cover"
  style="float: left; margin-right: 25px; max-width: 200px;" />
image from wikimedia, made by "frypie"


The first book that was recommended to me by my advisor for my PhD was "the emperor of all maladies". It was written by Siddhartha Mukherjee in 2010, won a Pulitzer prize and received many good reviews. It can be categorized as "popular science". The book spans 592 pages so I could write a much longer review, but I've chosen to only take what still stands out for me about 1.5 years since I've read the book. About 40% of us will develop to cancer in our lifetime and about 20% will succumb to it. I therefore _highly_ recommend the book. 

The book starts with the first known cases of cancer. It explains that cancers were called for being crabs, because it "hid" in the patient and suddenly launched out - much like a crab (keep in mind, they didn't do X-rays before). It then goes through the entire history of cancer, which unfortunately is quite horrid. Breast cancer is one example where doctors, who probably had the best of intentions, would remove the entire breast to treat cancers. This could happen with acid, ropes and knives. This in a time where anesthetics weren't that popular. More horror stories flourish throughout the book: the "radical oncology" which realized cancer exists beyond tumors, and thusly proceeded to remove as much tissue from patients as possible, i.e. surrounding muscles and tissue. Also the discovery of effective chemo that would make people throw up uncontrollably - before they discovered how to prevent some of it. There's a great, appropriate quote by Anna Deavere Smith, quoted in the book: _"Cancer therapy is like beating the dog with a stick to get rid of his fleas"_

Something that is cool throughout the book is how they discover some of the most effective treatments. Like a cloth dye applied to animal tissues that turned out to only highlight cells. Or cisplatin, that was supposed to stimulate bacterial cell division, but in fact did the opposite: it stopped cell division.

Another considerable portion of the book talks about "the war on cancer". It follows the life of several of the key spokes-figures and lobbyists. Here, too, there are some fun insights, like how a kid named Einar Gustafson became the front-figure of a campaign to raise funds - but they renamed him Jimmy.

The book also follows some of the patients a little closer, and as a reader you can understand some of the disease more. It is important to be aware the cancer usually follows long-term exposure to a mutagen. Common mutagens are alcohol and cigarette smoking. Other things can be carrying inflammations over extended time periods. The book also makes it clear that its just a matter of luck whether or not you BOTH get 1) mutated cells that keep on dividing and 2) mutated immuno-response that doesn't stop the division right away. 

Another insight that moved me a little is how eager patients are to try new drugs. There's a quote by Gracia Buffleben: "Dying people don't have time or energy . We can't keep doing this one woman, one drug, one company at a time". Doctors and researchers appear more hesitant, wanting to do tests and clinical trials. There have been cases where patients campaign for access to bleeding-edge medicine.

Yet another insight that also moved me more was just _how_ eager some of the doctors and surgeons were. Clinics were established were some doctors would experiment more or less unhindered on patients. In the early days were cancer was much more deadly than now, no one could object to experimentation. It really makes you feel sorry for everyone that has had the disease.

Fun fact to finish off: whales very rarely develop cancer. And they have waaaay more cells than us.
