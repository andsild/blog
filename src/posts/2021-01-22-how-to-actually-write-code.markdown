---
title: How to (actually) Write Code
---

[The mythical man month book](https://en.wikipedia.org/wiki/The_Mythical_Man-Month) suggests that productivity to programmers is about writing __programs__, not __programming products__. This also explains why many software projects are major undertakings; one only considers the time it would take to solve the __programming__ problems, not documenting code, testing, regressing, etc. By ignoring enterprise constraints, you're appealing more to the programmer heart.

Trying to write code without enterprise constraints can make one feel dirty. It's a common theme in job listsing to ask for publicly available code so that others can review your coding style. Therefore, a part of me always grudges to put code out in public that isn't clean or polished. I also don't want to show anyone code where I use anti-patterns, such as singletons, since it is considered "bad coding". Then again, a part of me is asking why I'd hide away - in the end I'd rather push out features than not. And when I obsess about 
dogmatic good programming practices I find that I often push less features than normal.

"Good" vs "bad code" is a _temporary_ matter of defintion. Code written in a rush and not refactored might appear messy, but chances are that the code is easy to interpret, since the author had no room to be clever. Minimized code, free of fluff, may appear better, but it may just be overly adapted for the current requirements. There will be more requirements later on. All the clever optimizations now leaves a lot of work, since minimized relies on assumptions. Traditional "Good code" may also exploit properties from the domain and use "techincallyCorrectVariableNames". This makes it better to a programmer with experience from the domain, but more difficult to someone outside, i.e. the next person to look at your code or yourself one year later.

Finally, I believe many developers (myself included) often overtune for efficiency. The holy grail of engineering. So we opt for the ten-year old framework running on stable tested machine. We write laborious code, no creativity: anything out of standard may cause unknown problems. This is so so so productive. And then we slowly burn out since our job lacks any form of personal expression.

 [Akin's Laws of Spacecraft Design](https://spacecraft.ssl.umd.edu/akins_laws.html) is one of my favorite web sites, and he puts what I'm trying to say much more succinct:
 14. (Edison's Law) "Better is the enemy of 'good'"
