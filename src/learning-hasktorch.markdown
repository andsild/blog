---
title: Hasktorch: Haskell implementation of Pytorch. Linear regression
---

Normally, when I learn a framework, I first download the code, make sure it works and then I try to implement a simple use case. But the more experienced I get the more I realize that reading the documentation might have certain benefits. It might tell you about the _intention_ of a framework, i.e. what it's supposed to do, but also what it is not supposed to do. This is especially important when you work with arcane languages as Haskell where a lot of frameworks are written by passionate volunteers who may not have the resources to handle everything.

So this time, I've decided to take on Hasktorch. It is a Pytorch implementation in Haskell. You can read more online: http://hasktorch.org/

In this post, I'll review their simple linear regression example listed on https://github.com/hasktorch/hasktorch/blob/master/examples/regression/Main.hs

Linear regression is simply trying to find a function that accurately represents a series of data. This usually becomes a mean function. The mean function may be _conditional_ if there is a correlation in your data: the chance for rain in March may e.g. higher than it is November. Or the number of ice creams eaten is higher during summer than it is in Winter. The month then becomes the condition that is pre-requesite for the mean. 

This is where AI comes in for linear regression. We may not be aware of the variables that affect our data. So instead of trying to create an accurate model, we unleash AI to estimate the linear model for us. It is important to note that a linear regression results in linear model. So, predicting a sine curve is not that easy.

The example from HaskTorch has an interesting way of defining the ground truth for a model.
```haskell
groundTruth :: Tensor -> Tensor
groundTruth t = squeezeAll $ matmul t weight + bias
  where
    weight = asTensor ([42.0, 64.0, 96.0] :: [Float])
    bias = full' [1] (3.14 :: Float)
```

Most examples with linear regression use a trigonometric or affine function as ground truth. Here, we are just stating what the model _should_ look like: a single linear layer with fixed weights and biases. The model is then defined as 
```haskell
model :: Linear -> Tensor -> Tensor
model state input = squeezeAll $ linear state input
```

The current model arcitechture is here referred to as `state`. The `state` refers to the weights and biases. I belive it is called `state` that because of its mutable nature: the weights and biases are subjects to change. Otherwise, I would've personally considered an alternative name like `m` or something else.

The `Linear` in this case simply refers to a model that has fully connected layers. The alternative are droput layers  where connections to neurons may be pruned or `Parallel` layer which requires synchronization steps between each layer. Its important for Haskell to know the differences in order to be able to _reason_ with the variable type. Right now it's a fully algebraic data type (ADT).

The training loop in HaskTorch's example looks like this:
```haskell
(trained, _) <- foldLoop (init, randGen) 2000 $ \(state, randGen) i -> do
        let (input, randGen') = randn' [batchSize, numFeatures] randGen
            (y, y') = (groundTruth input, model state input)
            loss = mseLoss y y'
        when (i `mod` 100 == 0) $ do
            putStrLn $ "Iteration: " ++ show i ++ " | Loss: " ++ show loss
        (state', _) <- runStep state GD loss 5e-3
        pure (state', randGen')
```

This examples makes it more clear why the model refers to itself as `state`. For each round of the training loop, we change the model, modifying it. It happens deterministically, so it is arguably not a single `state` variable, but a copy that is made each round.

The random input and random initialized state of the model is not determinstic, however. So everything happens inside of the IO monad.

For each round, random input is generated. The output from the model is then compared with the ground truth, which is a fixated model. The loss is simply the mean-squared error from model and ground truth output. Gradient descent is then used to tune the weights. Running the optimizer (in this case, `GD`) is actually implemented as a non-safe `IO` action.

The gradient descent `GD` has a simple implementation: `data GD = GD`. So it is just a type. But it has a class instance, "`step`" which is used to iterate along the loss curve. If you look at the example _above_ you'll see it near the bottom: `runStep state GD loss 5e-3`. The implementation of a step for `GD` is as follows:

```haskell
instance Optimizer GD where
  step lr gradients depParameters dummy = (gd lr gradients depParameters, dummy)
    where
      step p dp = p - (lr * dp)
      gd lr (Gradients gradients) parameters = zipWith step parameters gradients
```

We _could_ in theory make GD deterministic as well. But in `HaskTorch`'s case, it is bound in IO. The reason for this is the use C++ bindings from PyTorch.


To prove my own point, I decided to implement a simplex `GD` in Haskell.

