{-# LANGUAGE OverloadedStrings, ScopedTypeVariables, TupleSections, RecordWildCards, DeriveGeneric #-}

import Control.Monad (forM_)
import qualified Data.Char as Char
import Hakyll
import GHC.Exts (fromString)
import Data.Time.Clock.POSIX (posixSecondsToUTCTime)
import Data.Time.Clock (UTCTime)
import Text.Read (readMaybe)
import qualified Data.Aeson as Aeson -- For FromJSON
import GHC.Generics (Generic)
import Data.Yaml (decodeEither', ParseException)
import Data.String (fromString)
import qualified Data.Text as T
import qualified Data.Text.ICU.Normalize2 as ICUN
import qualified Data.Text.ICU.Char as ICUChar
import Data.ByteString.Char8 (pack)
import Debug.Trace (traceM)
import Text.Pandoc
  ( Extension (Ext_fenced_code_attributes, Ext_footnotes, Ext_gfm_auto_identifiers, Ext_implicit_header_references, Ext_smart),
    Extensions,
    ReaderOptions,
    WriterOptions (writerHighlightStyle),
    extensionsFromList,
    githubMarkdownExtensions,
    readerExtensions,
    writerExtensions,
  )
import Text.Pandoc.Highlighting (Style, breezeDark)

import Control.Monad (foldM, void)
import Data.List (stripPrefix, sortOn, (\\))
import Data.Maybe (fromMaybe, mapMaybe)
import qualified Data.Map.Strict as Map
import qualified Data.Tree as Tree
import System.FilePath.Posix (takeBaseName, takeDirectory, (</>)) 

--------------------------------------------------------------------------------
-- CONFIG
--

commentReplyToMap :: MonadMetadata m => [Identifier] -> m (Map.Map Identifier Identifier)
commentReplyToMap comments =
  foldM insertComment Map.empty comments
  where
    insertComment acc comment = do
      let maybePostSlug = stripPrefix "data/comments/" . takeDirectory . toFilePath $ comment
      case maybePostSlug of
        Nothing -> return acc
        Just postSlug -> do
          replyToID <- getMetadataField comment "reply_to"
          case replyToID of
            Nothing -> return acc
            Just "" -> return acc
            Just replyID -> do
              parentComments <- getMatches $ fromGlob $ "data/comments/" <> postSlug <> "/" <> replyID <> ".md"
              case parentComments of
                []              -> return acc
                [parentComment] -> return $ Map.insert comment parentComment acc
                _               -> error "Impossibru"

sortComments :: MonadMetadata m => [Item a] -> m [Item a]
sortComments items = do
  let itemIdentifierMap =
        foldl (\acc i -> Map.insert (itemIdentifier i) i acc) Map.empty items

  replyToMap <- commentReplyToMap . map itemIdentifier $ items
  let replyFromMap =
        Map.foldlWithKey (\acc k v -> Map.insertWith (++) v [k] acc) Map.empty replyToMap

  topLevelCommentIDs <- sortByDate $ Map.keys itemIdentifierMap \\ Map.keys replyToMap
  commentIDForest    <- flip Tree.unfoldForestM_BF topLevelCommentIDs $ \commentID ->
    (commentID,) <$> sortByDate (Map.findWithDefault [] commentID replyFromMap)

  return . map (itemIdentifierMap Map.!) . concatMap Tree.flatten $ commentIDForest
  where
    sortByDate xs = do
      datedItems <- mapM getDate xs
      return $ map fst . sortOn snd $ mapMaybe sequenceA datedItems

    getDate x = do
      date <- getMetadataField x "date"
      return (x, date)

root :: String
root =
  "https://qwde.no/blog"

siteName :: String
siteName =
  "Qwde Blog"

config :: Configuration
config =
  defaultConfiguration
    { destinationDirectory = "dist"
    , ignoreFile = const False
    , previewHost = "127.0.0.1"
    , previewPort = 8188
    , providerDirectory= "."
    , storeDirectory = "_cache"
    , tmpDirectory = "_tmp"
    }

-- Slugger wouldn't compile, so I copied code from: https://hackage.haskell.org/package/slugger-0.1.0.2/docs/src/Data.Text.Slugger.html
toSlug :: T.Text -> T.Text
toSlug = hyphenateWords . clean . normalize

clean :: T.Text -> T.Text
clean = T.foldr buildCleanText T.empty

normalize :: T.Text -> T.Text
normalize = ICUN.normalize ICUN.NFKD

buildCleanText :: Char -> T.Text -> T.Text
buildCleanText x acc
    | isCharModifier x || isSingleQuote x = acc
        | otherwise = T.concat [adjustChar x, acc]

adjustChar :: Char -> T.Text
adjustChar x
  | isAsciiAlphaNum x = toLowerAsText x
  | otherwise = " "

isAsciiAlphaNum :: Char -> Bool
isAsciiAlphaNum x = Char.isAscii x && Char.isAlphaNum x

toLowerAsText :: Char -> T.Text
toLowerAsText = T.singleton . Char.toLower


isSingleQuote :: Char -> Bool
isSingleQuote = (== '\'')

isCharModifier :: Char -> Bool
isCharModifier = ICUChar.property ICUChar.Diacritic
-- End block

hyphenateWords :: T.Text -> T.Text
hyphenateWords = T.intercalate (T.singleton '-') . T.words

urlPrefix :: String
urlPrefix = "blog/"

prefixedRoute :: Routes
prefixedRoute = customRoute $ (urlPrefix ++) . toFilePath

siteContext :: Context String
siteContext = defaultContext <> field "full_url" (const fullUrl)
  where
    fullUrl = do
      path <- getResourceFilePath
      return $ root <> case path of
        "./index.html" -> ""
        "./404.html"   -> "/404.html"
        _              -> drop 1 (takeDirectory path </> takeBaseName path <> "/")

data Comment = Comment
  { _id  :: String
  , date :: Int
  , email :: String
  , message :: String
  , name :: String
  , reply_to :: String
  , website :: String
  } deriving (Show, Generic)

instance Aeson.FromJSON Comment

-- Compiler function to parse the .yml file into a Comment type
compileYamlComment2 :: Compiler (Item String)
compileYamlComment2 = do
  content <- getResourceString
  let yamlBody = itemBody content
  let parsed = decodeEither' (pack $ yamlBody) :: Either ParseException String
  case parsed of
    Left err    -> fail $ "YAML parsing error: " ++ show err
    Right comm  -> return $ Item (itemIdentifier content) comm

compileYamlComment :: Compiler (Item Comment)
compileYamlComment = do
  content <- getResourceString
  let yamlBody = itemBody content
  let parsed = decodeEither' (pack yamlBody) :: Either ParseException Comment
  case parsed of
    Left err    -> fail $ "YAML parsing error: " ++ show err
    Right comm  -> return $ Item (itemIdentifier content) comm

-- Render the comment into HTML or plain text (you can modify this as needed)
commentCtx :: Context Comment
commentCtx = 
  field "id" (return . _id . itemBody) <>
  -- field "date" (return . show . date . itemBody) <>
  -- read date but render as UTCTime
  field "date" (return . show . parseDate . date . itemBody) <>
  field "email" (return . email . itemBody) <>
  field "message" (return . message . itemBody) <>
  field "name" (return . name . itemBody) <>
  boolField "reply_allowed" (const $ False) <>
  field "reply_to" (return . reply_to . itemBody) <>
  field "website" (return . website . itemBody) <>
  constField "level" "1"
  where 
    parseDate :: Int -> UTCTime
    parseDate = posixSecondsToUTCTime . fromIntegral

main :: IO ()
main = hakyllWith config $ do
  forM_
    [ "CNAME"
    , "favicon.ico"
    , "robots.txt"
    , "_config.yml"
    , "images/*"
    , "data/comments/*/*.yml"
    , "js/*"
    , "fonts/*"
    ]
    $ \f -> match (fromString f) $ do
      route prefixedRoute
      compile copyFileCompiler

  match ("templates/*") $
    compile templateBodyCompiler

  match (fromString $ "css/*") $ do 
    route prefixedRoute
    compile compressCssCompiler

  match ("_data/comments/*/*.yml") $ do
  -- match ("comment_dir/*.yml") $ do
    compile $ do 
      identifier <- getUnderlying
      traceM $ "Compiling comment: " ++ show identifier
      comment <- compileYamlComment 
      loadAndApplyTemplate "templates/comment.html" commentCtx comment
        >>= saveSnapshot "comment"

  match (fromList (map fromString ["about.markdown"])) $ do
      route $ prefixedRoute `composeRoutes` setExtension "html"
      compile $ pandocCompiler
          >>= loadAndApplyTemplate "templates/default.html" defaultContext
          >>= relativizeUrls

  match (fromString $ "posts/*") $ do
    route $ metadataRoute titleRoute `composeRoutes` prefixedRoute

    compile $ do
      path         <- getResourceFilePath
      let postSlug = takeBaseName path
      -- comments <- sortComments =<< loadAllSnapshots (fromGlob $ "_data/comments/" <> postSlug <> "/*.yml") "comment"
      comments <- loadAllSnapshots (fromGlob $ "_data/comments/" <> postSlug <> "/*.yml") "comment"
      
      let commentCount = show $ length comments
      traceM $ "Found " <> show commentCount <> " comments for " <> postSlug 
      traceM $ "Comments: " <> show comments

      void $ makeItem commentCount >>= saveSnapshot "comment_count"

      let ctx = constField "type" "article" <> 
                constField "post_slug" postSlug <>
                field "full_url" (const fullUrl) <>
                constField "comment_count" commentCount <>
                listField "comments" siteContext (return comments) <>
                boolField "comments_enabled" (const True) <>
                postCtx

      pandocCompilerCustom
        >>= loadAndApplyTemplate "templates/post.html" ctx
        >>= saveSnapshot "content"
        >>= loadAndApplyTemplate "templates/default.html" ctx
        >>= relativizeUrls

  match (fromString $ "index.html") $ do
    route prefixedRoute
    compile $ do
      posts <- recentFirst =<< loadAll "posts/*"

      let indexCtx =
            listField "posts" postCtx (return posts)
              <> constField "root" root
              <> constField "siteName" siteName
              <> defaultContext

      getResourceBody
        >>= applyAsTemplate indexCtx
        >>= loadAndApplyTemplate "templates/default.html" indexCtx

  create [fromString $ "sitemap.xml"] $ do
    route prefixedRoute
    compile $ do
      posts <- recentFirst =<< loadAll "posts/*"

      let pages = posts
          sitemapCtx =
            constField "root" root
              <> constField "siteName" siteName
              <> listField "pages" postCtx (return pages)

      makeItem ("" :: String)
        >>= loadAndApplyTemplate "templates/sitemap.xml" sitemapCtx

  create [fromString "rss.xml"] $ do
    route prefixedRoute
    compile (feedCompiler renderRss)

  create [fromString "atom.xml"] $ do
    route prefixedRoute
    compile (feedCompiler renderAtom)

  where
    fullUrl = do
      path <- getResourceFilePath
      return $ root <> drop 1 (takeDirectory path </> takeBaseName path <> "/")

feedCtx :: Context String
feedCtx =
  titleCtx
    <> postCtx
    <> bodyField "description"

postCtx :: Context String
postCtx =
  constField "root" root
    <> constField "siteName" siteName
    <> dateField "date" "%Y-%m-%d"
    <> defaultContext

titleCtx :: Context String
titleCtx =
  field "title" updatedTitle

replaceAmp :: String -> String
replaceAmp =
  replaceAll "&" (const "&amp;")

replaceTitleAmp :: Metadata -> String
replaceTitleAmp =
  replaceAmp . safeTitle

safeTitle :: Metadata -> String
safeTitle =
  fromMaybe "no title" . lookupString "title"

updatedTitle :: Item a -> Compiler String
updatedTitle =
  fmap replaceTitleAmp . getMetadata . itemIdentifier

pandocCompilerCustom :: Compiler (Item String)
pandocCompilerCustom =
  pandocCompilerWith pandocReaderOpts pandocWriterOpts

pandocExtensionsCustom :: Extensions
pandocExtensionsCustom =
  githubMarkdownExtensions
    <> extensionsFromList
      [ Ext_fenced_code_attributes
      , Ext_gfm_auto_identifiers
      , Ext_implicit_header_references
      , Ext_smart
      , Ext_footnotes
      ]

pandocReaderOpts :: ReaderOptions
pandocReaderOpts =
  defaultHakyllReaderOptions
    { readerExtensions = pandocExtensionsCustom
    }

pandocWriterOpts :: WriterOptions
pandocWriterOpts =
  defaultHakyllWriterOptions
    { writerExtensions = pandocExtensionsCustom
    , writerHighlightStyle = Just pandocHighlightStyle
    }

pandocHighlightStyle :: Style
pandocHighlightStyle =
  breezeDark -- https://hackage.haskell.org/package/pandoc/docs/Text-Pandoc-Highlighting.html

type FeedRenderer =
  FeedConfiguration ->
  Context String ->
  [Item String] ->
  Compiler (Item String)

feedCompiler :: FeedRenderer -> Compiler (Item String)
feedCompiler renderer =
  renderer feedConfiguration feedCtx
    =<< recentFirst
    =<< loadAllSnapshots "posts/*" "content"

feedConfiguration :: FeedConfiguration
feedConfiguration =
  FeedConfiguration
    { feedTitle = "Qwde Blog"
    , feedDescription = "Blog with small entries"
    , feedAuthorName = "Anders Sildnes"
    , feedAuthorEmail = "andsild@posteo.net"
    , feedRoot = root
    }

getTitleFromMeta :: Metadata -> String
getTitleFromMeta =
  fromMaybe "no title" . lookupString "title"

fileNameFromTitle :: Metadata -> FilePath
fileNameFromTitle =
  T.unpack . (`T.append` ".html") . toSlug . T.pack . getTitleFromMeta

titleRoute :: Metadata -> Routes
titleRoute =
  constRoute . fileNameFromTitle
