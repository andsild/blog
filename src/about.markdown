---
title: About this
---

I'm not making perfect posts or code on this blog. I find that I wouldn't bother publishing anything if I did. I just believe it is important for me to able to share my views, however brief, on ideas I like.

Qwde is a name that comes from mashing the keyboard. Maybe it will find its way to become something recursive and GNU-ish as "Qwde Was Deliberately Erratic".

Several posts in the blog are inspired by topics I've heard at <a href="https://opentodebate.org/">open to debate</a>. I recommend their podcasts, youtube videos and endeavor to improve civil conduct in debates.

# About me

I'm currently a PhD student at the university in Tromsø. I am studying computational pathology, a field that used digital solutions - mostly AI - to assist pathologists. The images that I look at are huge, so a part of my work is just trying to optimize pipelines for deep learning to make them faster.

You can reach me at <a href="http://www.linkedin.com/in/anders-sildnes"> linkedin</a>, or check out my profile at <a href="https://github.com/andsild/">github</a> or <a href="https://gitlab.com/andsild">gitlab</a>

The source code for the blog is hosted on [https://gitlab.com/andsild/blog](https://gitlab.com/andsild/blog).
